---
title: "README"
author: "Marco Molinari"
output: html_document
bibliography: references.bib
dorefs: true
---
# Bayesian Inference via Stochastic Gradient - Markov Chain Monte Carlo

This repository contains several routines to simulate data, perform Bayesian inference through Stochastic Gradient - Markov Chain Monte Carlo (SG-MCMC, [@zhang2019cyclical; @wenzel2020good]) and plot the results. The repository is structured as follows:

* **sgmcmc**: this is the main directory which contains the functions to build the model and perform inference.
* **data_simulation**: this directory contains functions to simulate data
* **plots**: this directory contains functions to produce summary plots of the posterior distributions of interest and convergence of the algorithm
* **experiments**: this directory contains the some simualtion experiments to determine the performance of the proposed models compared to the state-of-the-art (e.g. sg-mcmc vs hmc)

## References
<!-- REFERENCES -->