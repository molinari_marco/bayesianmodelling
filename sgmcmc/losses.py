"""
Losses from log-likelihood
"""
import logging

import tensorflow as tf
import tensorflow_probability as tfp

tfk = tf.keras
tfk.backend.set_floatx("float32")
tfd = tfp.distributions


class GaussianNegLogLikelihood:
    """
    Gaussian negative log-likelihood
    """

    def __init__(self, scale_is_trainable, default_scale=1.):
        """
        :param scale_is_trainable: whether the scale of the Gaussian is treated as an unknown random variable
        :type scale_is_trainable: bool
        :param default_scale: scale of the Gaussian distribution, default to 1.
        :type default_scale: float
        """
        self.scale_distribution = default_scale
        self.scale_is_trainable = scale_is_trainable

    @tf.function
    def sum_neg_log_likelihood(self, y_true, pred):
        """
        Negative log-likelihood of a Gaussian distribution with unknown mean (and/or unknown scale).

        :param y_true: true label value(s)
        :type y_true: tf.array (float)
        :param pred: predicted mean (and scale).
            If self.scale_is_trainable==True, then this array contains the predicted scales
            on the last column of the array.
        :type pred: tf.array (float)
        """
        if self.scale_is_trainable:
            # take the last column as the scale and all other ones as mean
            y_distr = tfd.Normal(loc=pred[:, 0:-1], scale=pred[:, -1:])
        else:
            y_distr = tfd.Normal(loc=pred, scale=self.scale_distribution)
        # return the sum of the negative log-likelihood
        return tf.reduce_sum(-y_distr.log_prob(y_true))


class BernoulliNegLogLikelihood:
    """
    Bernoulli negative log-likelihood
    """
    def __init__(self, parametrisation):
        """
        :param parametrisation: parametrisation of the Bernoulli random variable, 'logits' or 'probs'
        :type parametrisation: str
        """
        if parametrisation not in ['logits', 'probs']:
            logging.error("parametrisation must be one of ['logits', 'probs']")
            raise AttributeError("parametrisation must be one of ['logits', 'probs']")
        self.parametrisation = parametrisation

    @tf.function
    def sum_neg_log_likelihood(self, y_true, pred):
        """
        Sum negative log lik.

        :param y_true: true label value(s)
        :type y_true: tf.array (float)
        :param pred: predicted logits of probability of success
        :type pred: tf.array
        """
        if self.parametrisation == 'logits':
            y_distr = tfd.Bernoulli(logits=pred)
        else:
            y_distr = tfd.Bernoulli(probs=pred)
        return tf.reduce_sum(-y_distr.log_prob(y_true))


class LossTracking(tfk.metrics.Metric):
    """
    Class defining the loss, here the negative log-likelihood for a Gaussian rv
    """
    def __init__(self, name="Current_Loss", **kwargs):
        super(LossTracking, self).__init__(name=name, **kwargs)
        self.current_average = self.add_weight(name='current_average', initializer='zeros')
        self.number_of_calls = self.add_weight(name='n_calls', initializer='zeros')
    
    def update_state(self, y_true, y_pred=None, sample_weight=None):
        """
        Method that update the metric with a running average.
        
        Args:
            y_true: None, loss value
            y_pred:None, not needed
            sample_weight: None, not needed
        """
        self.number_of_calls.assign_add(1)
        new_average_to_add = tf.divide(tf.subtract(y_true, self.current_average),
                                       self.number_of_calls)
        self.current_average.assign_add(new_average_to_add)
    
    def result(self):
        return self.current_average
