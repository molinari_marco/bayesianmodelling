import numpy as np


class TrainTestSplit:
    """
    Split data given an index
    """
    def __init__(self, n, n_train, seed=1234):
        """
        :param n: sample size
        :type n: int
        :param n_train: train set size
        :type n_train: int
        :param seed: random seed
        :type seed: int
        """
        self.n = n
        self.n_train = n_train
        self.seed = seed
        #
        np.random.seed(seed)
        self.index_train = np.random.choice(n, size=n_train, replace=False)
        self.index_not_train = np.setdiff1d(np.arange(n), self.index_train)

    def train_test_split(self, input_data):
        """
        Creates a train-test split with a random index.
        Return a dictionary with the same keys and two dataframes/arrays for each key for test and train.

        :param input_data: dictionary with input dataframes, pandas or numpy
        :type input_data: dict
        :return: dictionary with the splitted dataframes dataframes
        :rtype: dict
        """
        dict_out = dict()
        for key, df in input_data.items():
            dict_out[key] = dict(train=df[self.index_train],
                                 test=df[self.index_not_train])

        return dict_out
