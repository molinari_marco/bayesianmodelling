"""
Collect weights MC samples during training
"""
import numpy as np
import tensorflow as tf


class GetPosterior:
    """
    This class is used to collect the samples from the posterior distribution of the weights during the MCMC training.
    Or the running average of the parameters (like mean and standard deviation of a Normal distribution)
    """
    
    def __init__(self, n_samples, effective_sample_size, model, layers_names, store_running_average=True,
                 store_full_chain=False):
        self.n_samples = n_samples
        self.effective_sample_size = effective_sample_size
        self.model = model
        self.layers_names = layers_names
        self.store_full_chain = store_full_chain
        self.store_running_average = store_running_average
        #
        self.dict_keys = list()
        self.posterior_samples = dict()
        self.run_ave = list()
        self.run_ave_1 = list()
        self.run_ave_square = list()
        self.run_ave_square_1 = list()
        self.iteration_fill = 0
        self.iteration_average = 0

        if store_full_chain:
            for layer in self.layers_names:
                for pos, ww in enumerate(self.model.get_layer(layer).get_weights()):
                    self.dict_keys.append(f"{layer}_pos_{pos}")
                    self.posterior_samples[f"{layer}_pos_{pos}"] = np.zeros(
                        shape=(self.effective_sample_size,) + ww.shape, dtype=np.float32
                    )

        if store_running_average:
            for pos, ww in enumerate(self.model.get_weights()):
                self.run_ave.append(
                    np.zeros(shape=ww.shape, dtype=np.float32)
                    )
                self.run_ave_square.append(
                    np.zeros(shape=ww.shape, dtype=np.float32)
                    )
                self.run_ave_1.append(
                    np.zeros(shape=ww.shape, dtype=np.float32)
                    )
                self.run_ave_square_1.append(
                    np.zeros(shape=ww.shape, dtype=np.float32)
                    )

    def one_step_fill(self, weight_sample):
        if self.store_full_chain:
            for pos, ww in enumerate(weight_sample):
                self.posterior_samples[self.dict_keys[pos]][self.iteration_fill] = ww.numpy()
            self.iteration_fill += 1
        if self.store_running_average:
            self.running_average(weight_sample)

    def running_average(self, weight_sample):
        average_iter = self.iteration_average + 1
        for pos, ww in enumerate(weight_sample):
            ww_power = tf.math.multiply(ww, ww)
            # update running average at time t
            self.run_ave[pos] = tf.math.divide(
                self.run_ave_1[pos] * self.iteration_average + ww, average_iter)
            self.run_ave_square[pos] = tf.math.divide(
                self.run_ave_square_1[pos] * self.iteration_average + ww_power, average_iter)
            # update running average at time t-1
            self.run_ave_1[pos] = self.run_ave[pos]
            self.run_ave_square_1[pos] = self.run_ave_square[pos]
        self.iteration_average += 1
