"""
Custom Learning rate schedule functions
"""
import numpy as np
import tensorflow as tf
from tensorflow.keras.optimizers.schedules import LearningRateSchedule
from tensorflow.python.ops import math_ops

tfk = tf.keras
tfk.backend.set_floatx("float32")


class PolynomialDecay(LearningRateSchedule):
    """
    A polynomial decay learning rate schedule
    """
    
    def __init__(self,
                 initial_learning_rate,
                 decay_steps,
                 end_learning_rate,
                 power=1.0,
                 name=None):
        super(PolynomialDecay, self).__init__()
        
        self.initial_learning_rate = initial_learning_rate
        self.decay_steps = decay_steps
        self.end_learning_rate = end_learning_rate
        self.power = power
        self.name = name
        self.diff_lr = initial_learning_rate - end_learning_rate
    
    def __call__(self, step):
        current_step = math_ops.minimum(step, self.decay_steps)
        step_ratio = math_ops.divide(current_step, self.decay_steps)
        
        lr_t = math_ops.add(
            math_ops.multiply(self.diff_lr, math_ops.pow(1 - step_ratio, self.power)),
            self.end_learning_rate
            )
        
        return lr_t


class CyclicalDecay(LearningRateSchedule):
    """
    A cyclical learning rate schedule
    """
    
    def __init__(self,
                 initial_learning_rate,
                 tot_steps,
                 num_cycles,
                 name=None):
        """
        Args:
            initial_learning_rate: float. Initial learning rate
            tot_steps: int. Total number of training steps
            num_cycles: int. Number of cycles
            name: str or None. Optional name of the object
        """
        super(CyclicalDecay, self).__init__()
        
        self.initial_learning_rate = initial_learning_rate
        self.tot_steps = tot_steps
        self.num_cycles = num_cycles
        self.name = name
        
        self.steps_per_cycle = tot_steps // num_cycles
    
    def __call__(self, step):
        """
        Return the learning rate at the current step following the cyclical schedule
        Args:
            step: the current training step
        """
        cos_numerator = np.pi * (step % self.steps_per_cycle)
        cos_arg = cos_numerator / self.steps_per_cycle
        
        alpha_step = 0.5 * self.initial_learning_rate * (math_ops.cos(cos_arg) + 1)
        
        return alpha_step
