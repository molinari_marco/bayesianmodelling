"""
Main training loop with SG-MCMC
"""
import time

import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow.python.framework import ops

from sgmcmc.utility.posterior_samples_collect import GetPosterior
from sgmcmc.losses import LossTracking

tfk = tf.keras
tfk.backend.set_floatx("float32")
tfd = tfp.distributions


class SGMCMC:
    """
    Main object containing all details of the MCMC
    """
    def __init__(self, model, label_distribution, n_epochs, data_train_generator, data_test_generator, sampler,
                 n_batches, thinning, lr_schedule, temperature_exploration=1., temperature_sampling=1.,
                 threshold_collect=0, store_running_average=False, store_full_chain=True, metrics=None):
        """
        Define the class.

        :param model: The model built with Keras layers
        :type model: tf.keras.model
        :param label_distribution: Probability distribution of the label
        :type label_distribution: sgmcmc.utility.losses.NegLogLikelihood
        :param n_epochs: Number of training epochs
        :type n_epochs: int
        :param n_batches: Number of mini batches
        :type n_batches: int
        :param data_train_generator: tf mini-batch generator for the train features and label
        :type data_train_generator: tf.data.Dataset
        :param data_test_generator: tf mini-batch generator for the train features and label
        :type data_test_generator: tf.data.Dataset
        :param sampler: Stochastic Gradient - MCMC sampler
        :type sampler: tf.keras.Optimizers
        :param thinning: Thinning value, how often we store the posterior samples
        :type thinning: int
        :param lr_schedule: Learning rate schedule to use
        :type lr_schedule: tf.keras.optimizers.schedules derived classes
        :param temperature_exploration: Temperature to use during the exploration stage
        :type temperature_exploration: float
        :param temperature_sampling: Temperature to use during the sampling stage
        :type temperature_sampling: float
        :param threshold_collect: Learning rate threshold after which the posterior samples are collected,  in(0, 1)
        :type threshold_collect: float
        :param store_running_average: If True only a running mean and variance of the parameters is stored
        :type store_running_average: bool
        :param store_full_chain: If True the whole Markov chain is stored
        :type store_full_chain: bool
        :param metrics: Define the metric to compute during training
        :type metrics: tf.keras.Metrics
        """
        self.model = model
        self.label_distribution = label_distribution
        self.n_epochs = n_epochs
        self.n_batches = n_batches
        self.data_train_generator = data_train_generator
        self.data_test_generator = data_test_generator
        self.sampler = sampler
        self.thinning = thinning
        self.store_running_average = store_running_average
        self.store_full_chain = store_full_chain
        self.metrics = metrics
        #
        self.lr_schedule = lr_schedule
        # get the two temperatures
        self.temperature_exploration = temperature_exploration
        self.temperature_sampling = temperature_sampling
        self.comp_time = None
        # transform the threshold to reflect the learning rate from the cyclical schedule
        self.threshold_collect = ops.convert_to_tensor_v2(
            0.5 * lr_schedule.initial_learning_rate * (np.cos(threshold_collect * np.pi) + 1),
            dtype=tf.float32)
        # Posterior samples container
        # Need to calculate the final sample size based on threshold_collect
        iter_per_cycle = (n_batches * n_epochs) // lr_schedule.num_cycles
        n_samples = 0
        for ii in range(iter_per_cycle):
            n_samples += (lr_schedule(ii) < self.threshold_collect).numpy()
        
        self.n_samples = n_samples * lr_schedule.num_cycles
        self.effective_sample_size = int(self.n_samples / thinning)
        self.thinning_counter = 0
        self.layers_name = [ll.name for ll in self.model.layers[1:]]

        self.posterior_distribution = GetPosterior(
            n_samples=self.n_samples,
            effective_sample_size=self.effective_sample_size,
            model=model,
            layers_names=self.layers_name,
            store_running_average=self.store_running_average,
            store_full_chain=self.store_full_chain
        )

        # Prepare the metrics class
        if self.metrics is not None:
            self.history_metrics = dict(
                train=np.zeros([n_epochs]),
                val=np.zeros([n_epochs])
            )

            self.train_metric = self.metrics(name="train")
            self.val_metric = self.metrics(name="val")

        # Prepare the loss class
        # containers for loss and metrics
        self.history_loss = dict(
            train=np.zeros([n_epochs]),
            val=np.zeros([n_epochs])
            )

        self.train_loss = LossTracking(name="train_loss")
        self.val_loss = LossTracking(name="val_loss")
        
        # Define a Standard Normal distribution to generate the random errors for the weights update
        self.std_normal = tfp.distributions.Normal(loc=0., scale=1.)
    
    # @tf.function
    def train_step(self, x, y):
        """
        This method defines one training step on a mini-batch.

        :param x: Input features batch in the form of a dictionary of tf.Tensor
        :type x: dict
        :param y: Input label batch
        :type y: tf.Tensor
        """
        # Open a GradientTape to record the operations run
        # during the forward pass, which enables auto-differentiation
        with tf.GradientTape() as tape:
            output = self.model(x, training=True)
            loss_value = self.label_distribution.sum_neg_log_likelihood(y_true=y, pred=output)
            # Add the loss coming from the prior distribution
            loss_value += tf.reduce_sum(self.model.losses)
        # Compute the gradients
        current_weights = self.model.trainable_weights
        grads = tape.gradient(loss_value, current_weights)
        self.sampler.apply_gradients(zip(grads, current_weights))
        
        current_weights = self.model.trainable_weights
        lr_t = self.sampler.current_lr
        # Check the current learning rate to decide when to start storing samples
        if lr_t < self.threshold_collect:
            for weights_array in current_weights:
                weights_array.assign_add(
                    tf.sqrt(0.5) * lr_t * self.temperature_sampling * self.std_normal.sample(weights_array.shape)
                    )
            # Store the weights
            self.thinning_counter += 1
            if (self.thinning_counter % self.thinning) == 0:
                self.posterior_distribution.one_step_fill(weight_sample=current_weights)
        
        # if the learning rate is above the threshold, do not collect
        # and use the temperature_exploration
        elif lr_t >= self.threshold_collect:
            for weights_array in current_weights:
                weights_array.assign_add(
                    tf.sqrt(0.5) * lr_t * self.temperature_exploration * self.std_normal.sample(weights_array.shape)
                    )
        if self.metrics is not None:
            # Update the metric
            self.train_metric.update_state(y, output)
        # Update the loss
        self.train_loss.update_state(y_true=loss_value)
        
        # return the loss
        return loss_value
    
    # @tf.function
    def val_step(self, x, y):
        output = self.model(x, training=False)
        val_loss_value = self.label_distribution.sum_neg_log_likelihood(y_true=y, pred=output)
        val_loss_value += tf.reduce_sum(self.model.losses)
        if self.metrics is not None:
            # Update the metric
            self.val_metric.update_state(y, output)
        # Update the loss
        self.val_loss.update_state(y_true=val_loss_value)
        
        return val_loss_value

    def run_chain(self):
        """
        Train the model
        """
        
        # Training loop
        start_time = time.time()
        for epoch in range(self.n_epochs):
            
            print(f"========== Training Epoch = {epoch} ===========")
            
            # Iterate over the batches of the dataset
            for x_batch_train, y_batch_train in self.data_train_generator:
                self.train_step(
                    x=x_batch_train,
                    y=y_batch_train
                    )
            # At the end of each epoch display the metrics
            if self.metrics is not None:
                self.history_metrics["train"][epoch] = self.train_metric.result()
                print("Training metric: %.4f" % (float(self.train_metric.result()),))
            self.history_loss["train"][epoch] = self.train_loss.result()
            print("Training loss: %.4f" % (float(self.train_loss.result()),))
        
            # Run a validation loop at the end of each epoch.
            for x_batch_val, y_batch_val in self.data_test_generator:
                self.val_step(
                    x=x_batch_val,
                    y=y_batch_val
                    )
            if self.metrics is not None:
                self.history_metrics["val"][epoch] = self.val_metric.result()
                print("Validation metric: %.4f" % (float(self.val_metric.result()),))
            self.history_loss["val"][epoch] = self.val_loss.result()
            print("Validation loss: %.4f" % (float(self.val_loss.result()),))

            # Reset metrics at the end of each epoch
            if self.metrics is not None:
                self.train_metric.reset_states()
                self.val_metric.reset_states()
            # Reset losses at the end of each epoch
            self.train_loss.reset_states()
            self.val_loss.reset_states()
        
        end_time = time.time()
        self.comp_time = end_time - start_time
        print("Time taken: %.2fs" % self.comp_time)

    def predict(self, x):
        """
        Predictions based on new features. Function to be used after training.

        x = tf.tensor. The tensor with the new features
        """
        # create predictions output array
        predictions = np.zeros([self.effective_sample_size, x.shape[0], self.model.output.shape[1]])
        param_names = self.posterior_distribution.dict_keys
        for sample in range(self.effective_sample_size):
            posteior_weights = [
                self.posterior_distribution.posterior_samples[layer_param][sample] for layer_param in param_names]
            self.model.set_weights(posteior_weights)
            predictions[sample, :] = self.model(x)
        return predictions

    def online_predict(self, x):
        """
        Predictions based on new features. Function to be used while training.

        x = tf.tensor. The tensor with the new features
        """
        prediction = self.model(x)
        return prediction
