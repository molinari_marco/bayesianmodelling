"""
SGLD MCMC
"""
import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import array_ops
from tensorflow.python.framework import ops

tfk = tf.keras
tfk.backend.set_floatx("float32")
tfd = tfp.distributions


class SGLD(tfk.optimizers.Optimizer):
    
    def __init__(self, learning_rate=0.01, name="BayesianOptimizer", **kwargs):
        super(SGLD, self).__init__(name=name, **kwargs)
        self._set_hyper("learning_rate", kwargs.get("lr", learning_rate))
        self.std_normal = tfd.Normal(0., 1.)
    
    def _create_slots(self, var_list):
        pass

    @tf.function
    def _resource_apply_dense(self, grad, var, apply_state=False):
        """
        Perform the update of the weights with SGLD
        """
        # var_device, var_dtype = var.device, var.dtype.base_dtype
        # coefficients = ((apply_state or {}).get((var_device, var_dtype)) or 
        #                 self._fallback_apply_state(var_device, var_dtype))
        var_dtype = var.dtype.base_dtype
        # update the learning rate given some decaying schedule
        lr_t = self._decayed_lr(var_dtype)
        
        # standard Gradient Descent update rule
        new_var = var - lr_t * grad + math_ops.sqrt(2.*lr_t) * self.std_normal.sample(var.shape)
        
        # Update the value of var
        var.assign(new_var)
    
    def get_config(self):
        base_config = super(SGLD, self).get_config()
        return {
            **base_config,
            "learning_rate": self._serialize_hyperparameter("learning_rate"),
        }
