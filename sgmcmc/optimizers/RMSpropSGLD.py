"""
RMSprop preconditioned SGLD MCMC
"""
import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import array_ops
from tensorflow.python.framework import ops

tfk = tf.keras
tfk.backend.set_floatx("float32")
tfd = tfp.distributions


class RMSpropSGLD(tfk.optimizers.Optimizer):
    """
    This optimizer class provides a SGLD with preconditioning.
    The precondition matrix is a diagonal matrix adaptively calculated
    following the same procedure of the RMSprop SGD.
    
    Args:
        learning_rate: Tensor or float. The global learning rate of the SGD
        rho: float. The discount factor for the moving avarage on the gradient second moment
        tempering: float or function. Tempering schedule to use on the variable updates
        epsilon: float. Small constant to avoid overflow
    """
    def __init__(self,
                 learning_rate=0.01,
                 rho=0.9,
                 epsilon=1e-7,
                 name="BayesianOptimizer",
                 **kwargs):
        super(RMSpropSGLD, self).__init__(name=name, **kwargs)
        self._set_hyper("learning_rate", kwargs.get("lr", learning_rate))
        self._set_hyper("rho", rho)
        self.epsilon = epsilon
        self.std_normal = tfd.Normal(0., 1.)
        # create a holder for the current learning rate
        self.current_lr = None
    
    def _create_slots(self, var_list):
        for var in var_list:
            self.add_slot(var, "rms")
    
    def _prepare_local(self, var_device, var_dtype, apply_state):
        super(RMSpropSGLD, self)._prepare_local(var_device, var_dtype, apply_state)
        
        rho = array_ops.identity(self._get_hyper("rho", var_dtype))
        apply_state[(var_device, var_dtype)].update(
            dict(
                epsilon=ops.convert_to_tensor_v2(self.epsilon, var_dtype),
                rho=rho,
                one_min_rho=1. - rho
                )
            )

    # @tf.function
    def _resource_apply_dense(self, grad, var, apply_state=False):
        """
        Perform the update of the weights with a RMSprop preconditioned SGLD
        """
        var_dtype = var.dtype.base_dtype
        var_device = var.device
        # update the learning rate given some decaying schedule
        lr_t = self._decayed_lr(var_dtype)
        self.current_lr = lr_t
        # get the other coefficients
        coefs = (apply_state.get((var_device, var_dtype)))
        # get the rms variable
        rms = self.get_slot(var, "rms")
        
        # moving average of square gradients
        rms_t = coefs["rho"] * rms + coefs["one_min_rho"] * math_ops.square(grad)
        # save the new square grads
        rms.assign(rms_t)
        # SGLD update rule with RMSprop
        new_lr = lr_t / (math_ops.sqrt(rms_t) + coefs["epsilon"])
        # new_var = var - new_lr * grad + math_ops.sqrt(2.*new_lr) * self.std_normal.sample(var.shape)
        new_var = var - new_lr * grad
        
        # Update the value of var
        var.assign(new_var)
    
    def get_config(self):
        base_config = super(RMSpropSGLD, self).get_config()
        base_config.update({
            "learning_rate": self.learning_rate,
            "rho": self.rho,
            "epsilon": self.epsilon
            })
        return base_config
