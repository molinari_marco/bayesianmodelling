"""
Custom Keras Layers
"""
import tensorflow as tf
import tensorflow_probability as tfp

tfk = tf.keras
tfk.backend.set_floatx("float32")
tfd = tfp.distributions

# global variable layers_names
layers_names = list()


# Custom Dense Layer
class BayesianDense(tfk.layers.Dense):
    """
    Bayesian Dense layer with prior distributions over the weights.
    """
    global layers_names

    def __init__(self, units, n_batches, kernel_prior, name, bias_prior=None, bijector_kernel=None, bijector_bias=None,
                 activation=None, use_bias=True, **kwargs):
        """
        :param units: number of output units (neurons)
        :type units: int
        :param n_batches: number of batches
        :type n_batches: int
        :param kernel_prior: prior distribution for the kernel weights
        :type kernel_prior: tensorflow_probability.python.distributions.distribution.Distribution
        :param bias_prior: prior distribution for the bias (intercept)
        :type bias_prior: tensorflow_probability.python.distributions.distribution.Distribution
        :param bijector_kernel: kernel bijector
        :type bijector_kernel: tfp.bijectors.Bijector
        :param bijector_bias: bias bijector
        :type bijector_bias: tfp.bijectors.Bijector
        :param name: name of the layer
        :type name: str
        """
        self.kernel_prior = kernel_prior
        self.bias_prior = bias_prior
        self.n_batches = n_batches
        # If the bijector in input is None, use the Identity bijector
        if bijector_kernel is None:
            self.bijector_kernel = tfp.bijectors.Identity()
        else:
            self.bijector_kernel = bijector_kernel
        if bijector_bias is None:
            self.bijector_bias = tfp.bijectors.Identity()
        else:
            self.bijector_bias = bijector_bias
        #
        self.kernel = None
        self.kernel_transformed = None
        self.bias = None
        self.bias_transformed = None
        # check that name is not already being used for another layer
        if name in layers_names:
            raise ValueError(f"layer name {name} already used, change it")
        layers_names.append(name)
        super(BayesianDense, self).__init__(units=units, activation=activation, use_bias=use_bias, name=name, **kwargs)

    def build(self, input_shape):
        self.kernel = self.add_weight(shape=[input_shape[-1], self.units])
        if self.use_bias:
            self.bias = self.add_weight(shape=[self.units])

    def call(self, inputs):
        # check if a bijector is needed
        self.kernel_transformed = self.bijector_kernel(self.kernel)
        # calculate the loss given the prior distribution
        kernel_loss = tf.reduce_sum(-self.kernel_prior.log_prob(self.kernel_transformed))
        # add the losses to the layer
        self.add_loss(tf.math.divide(kernel_loss, self.n_batches))

        # for the bias
        if self.use_bias:
            self.bias_transformed = self.bijector_kernel(self.bias)
            bias_loss = tf.reduce_sum(-self.bias_prior.log_prob(self.bias_transformed))
            self.add_loss(tf.math.divide(bias_loss, self.n_batches))

        # compute the layer output using built-in call method
        outputs = super(BayesianDense, self).call(inputs)
        return outputs
