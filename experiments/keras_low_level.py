"""
Building  and training a simple NN from TF-Keras low-level API
"""
import tensorflow as tf
import tensorflow_probability as tfp
import numpy as np
import matplotlib.pyplot as plt

tfk = tf.keras
tfk.backend.set_floatx("float32")
tfd = tfp.distributions


def loss_plot(model_history):
    plt.figure()
    train_loss = np.array(model_history.history["loss"])
    val_loss = np.array(model_history.history["val_loss"])
    x = [x + 1 for x in range(len(train_loss))]
    
    plt.plot(x, train_loss, label="Train Loss")
    plt.plot(x, val_loss, label="Validation Loss")
    
    plt.legend(loc="upper right")
    plt.xlabel("epochs")
    plt.ylabel("loss")
    plt.show()


# Simulate some data from a linear regression
n = 1000
p = 5

np.random.seed(123)
X = np.random.randn(n, p)
true_weights = np.array([1, -1, 2, -1.5, -0.5])
y = X.dot(true_weights) + np.random.randn(n)

# Split in Train and Test
n_train = int(0.7 * n)
n_test = n - n_train

# SGD parameters
n_epochs = 20
batch_size = 64
n_batches = np.ceil(n_train / batch_size)

# Define TensorFlow Dataset instance - produces mini-batches
data = tf.data.Dataset.from_tensor_slices((X, y))
data = data.shuffle(n_train, reshuffle_each_iteration=True)
#
data_train = data.take(n_train).batch(batch_size).repeat(n_epochs)
data_test = data.skip(n_train).batch(1).repeat(n_epochs)

"""
Standard Machine Learning linear model
"""

input_layer = tfk.Input(shape=(p, ))
out_layer = tfk.layers.Dense(units=1, activation=None)(input_layer)

model = tfk.models.Model(input_layer, out_layer)


def neg_log_likelihood(y_true, y_pred):
    y_distr = tfd.Normal(loc=y_pred, scale=1.)
    return -tf.reduce_mean(y_distr.log_prob(y_true))


model.compile(
    optimizer=tf.optimizers.RMSprop(learning_rate=0.01),
    loss=neg_log_likelihood,
    metrics="mse"
    )

model.fit(
    data_train,
    epochs=n_epochs,
    validation_data=data_test,
    verbose=1
    )

loss_plot(model_history=model.history)
model.get_weights()



