"""
Simulation
"""
import tensorflow as tf
import tensorflow_probability as tfp
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import data_simulation.batch_generator
from sgmcmc.create_mcmc import SGMCMC
from data_simulation import linear_models
from sgmcmc import custom_layers, learning_rate_schedule
from sgmcmc.optimizers import RMSpropSGLD
from plots import loss_plot

tfk = tf.keras
tfk.backend.set_floatx("float32")
tfd = tfp.distributions

# Generate some data from a linear regression
n = 5000
p = 5
n_train_prop = 0.8

dict_data = linear_models.sim_linear_regression(n, n_train_prop, p, seed=1234)

# mini-batch settings
batch_size = 128
n_epochs = 50
n_batches = int(np.ceil((n * n_train_prop) / batch_size))

dict_batch_generators = data_simulation.batch_generator.data_batch_generator(dict_data, batch_size)

# Define the model
input_layer = tfk.Input(shape=(p, ))
out_layer = custom_layers.BayesianDense(units=1, n_batches=n_batches, kernel_prior=, name=None, bias_prior=,
                                        activation=None, prior_mean=0., prior_scale=1.)(input_layer)
model = tfk.Model(input_layer, out_layer)

# Create arrays to store the MC of the weights
tot_iterations = int(n_epochs * n_batches)
thinning = 1

lr_fn = learning_rate_schedule.CyclicalDecay(
    initial_learning_rate=0.1,
    tot_steps=tot_iterations,
    num_cycles=2
    )

sampler = RMSpropSGLD(
    learning_rate=lr_fn,
    rho=0.95)

# Define the MCMC object
mcmc = SGMCMC(model=model, label_distribution=None, n_epochs=n_epochs,
              data_train_generator=dict_batch_generators["data_train"],
              data_test_generator=dict_batch_generators["data_test"], sampler=sampler, n_batches=n_batches,
              thinning=thinning, lr_schedule=lr_fn, temperature_exploration=0.5, temperature_sampling=1.,
              threshold_collect=0.5)

# Check the number of effective samples
print(mcmc.threshold_collect)
print(mcmc.n_samples)

# run the chain
mcmc.run_chain()

# Plot the loss and metric
loss_plot.loss_plot(
    model_history=mcmc.history_metrics,
    label_text="metric"
    )

loss_plot.loss_plot(
    model_history=mcmc.history_loss,
    label_text="loss",
    train_sample_size=n * n_train_prop,
    test_sample_size=n * (1-n_train_prop)
)

weights = mcmc.posterior_distribution.posterior_samples[0]
bias = mcmc.posterior_distribution.posterior_samples[1]
plt.plot(weights[:, :, 0])

# With seaborn
col_names = [f"w{ii}" for ii in range(p)]
df_weights = pd.DataFrame(data=weights[:, :, 0], columns=col_names)

sns.set_style("darkgrid")
sns.jointplot(data=df_weights, x="w0", y="w1", kind='kde', fill=True)
sns.jointplot(data=df_weights, x="w1", y="w2", kind='kde', fill=True)

jointplot = sns.jointplot(data=df_weights, x="w0", y="w1", kind='kde', fill=True)
fig = jointplot.fig
fig.savefig('density01.png', dpi=250)

print(dict_data["true_weights"])


"""
Comparison with Standard Hamiltonian Monte-Carlo
"""
y_train = dict_data["y_train"]
X_train = dict_data["X_train"]

# Prior on Beta
rv_beta = tfd.MultivariateNormalDiag(
    loc=tf.zeros((p), dtype=tf.float32),
    scale_identity_multiplier=1.,
    name="rv_beta"
)


# Define the Un-normalised Joint Posterior Log
def unnormalized_log_prob(beta):
    # Define model likelihood
    rv_observations = tfd.Normal(
        loc=tf.tensordot(X_train, beta, axes=1),
        scale=1.
    )

    # Log probabilities of priors.
    # Notice that each prior should be divided by num_samples and
    # likelihood is divided by batch_size for pSGLD optimization.
    prior_loss = rv_beta.log_prob(beta)
    loglik = tf.reduce_sum(rv_observations.log_prob(y_train[:, 0]))

    sum_log_prob = prior_loss + loglik
    return sum_log_prob


# Initialize the HMC transition kernel.
num_results = int(10e3)
num_burnin_steps = int(1e3)
adaptive_hmc = tfp.mcmc.SimpleStepSizeAdaptation(
    tfp.mcmc.HamiltonianMonteCarlo(
        target_log_prob_fn=unnormalized_log_prob,
        num_leapfrog_steps=3,
        step_size=1.),
    num_adaptation_steps=int(num_burnin_steps * 0.8))

initial_state = [
    tf.random.normal(shape=(p,), dtype=tf.float32, name="beta"),
    ]


@tf.function
def run_chain():
    # Run the chain (with burn-in).
    samples, is_accepted = tfp.mcmc.sample_chain(
      num_results=num_results,
      num_burnin_steps=num_burnin_steps,
      current_state=initial_state,
      kernel=adaptive_hmc,
      trace_fn=lambda _, pkr: pkr.inner_results.is_accepted
      )

    is_accepted = tf.reduce_mean(tf.cast(is_accepted, dtype=tf.float32))
    return samples, is_accepted


samples, is_accepted = run_chain()

col_names = [f"w{ii}" for ii in range(p)]
df_weights_hmc = pd.DataFrame(data=samples[0].numpy(), columns=col_names)

sns.set_style("darkgrid")

jointplot = sns.jointplot(data=df_weights_hmc, x="w0", y="w1", kind='kde', fill=True)
fig = jointplot.fig
fig.savefig('hcm_density01.png', dpi=250)

jointplot = sns.jointplot(data=df_weights_hmc, x="w1", y="w2", kind='kde', fill=True)
fig = jointplot.fig
fig.savefig('hcm_density12.png', dpi=250)

jointplot = sns.jointplot(data=df_weights_hmc, x="w2", y="w3", kind='kde', fill=True)
fig = jointplot.fig
fig.savefig('hcm_density23.png', dpi=250)
