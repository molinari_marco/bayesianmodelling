"""
Custom Dense layer with prior distribution on weights and associated loss
"""
import time

import tensorflow as tf
import tensorflow_probability as tfp
import numpy as np
import matplotlib.pyplot as plt

tfk = tf.keras
tfk.backend.set_floatx("float32")
tfd = tfp.distributions


def loss_plot(model_history):
    plt.figure()
    train_loss = np.array(model_history["train"])
    val_loss = np.array(model_history["val"])
    x = [x + 1 for x in range(len(train_loss))]
    
    plt.plot(x, train_loss, label="Train Loss")
    plt.plot(x, val_loss, label="Validation Loss")
    
    plt.legend(loc="upper right")
    plt.xlabel("epochs")
    plt.ylabel("loss")
    plt.show()


# Simulate some data from a linear regression
n = 500
p = 5

np.random.seed(123)
X = np.random.randn(n, p).astype(np.float32)
true_weights = np.array([1, -1, 2, -1.5, -0.5]).astype(np.float32)
y = X.dot(true_weights) + np.random.randn(n).astype(np.float32)
y = np.reshape(y, newshape=[n, 1])

# Split in Train and Test
n_train = int(0.8 * n)
n_test = n - n_train

# SGD parameters
n_epochs = 10
batch_size = 32
n_batches = np.ceil(n_train / batch_size)

# Define TensorFlow Dataset instance - produces mini-batches
data = tf.data.Dataset.from_tensor_slices((X, y))
data = data.shuffle(n, reshuffle_each_iteration=True)
#
data_train = data.take(n_train).batch(batch_size).repeat(n_epochs)
data_test = data.skip(n_train).batch(1).repeat(n_epochs)


"""
Standard Machine Learning linear model
"""

@tf.function
def neg_log_likelihood(y_true, y_pred):
    y_distr = tfd.Normal(loc=y_pred, scale=1.)
    return tf.reduce_mean(-y_distr.log_prob(y_true))


class CustomLoss(tfk.metrics.Metric):
    """
    Class defining the loss, here the neg log likelihood for a Gaussian
    """
    def __init__(self, name="Gaussian_Loss", **kwargs):
        super(CustomLoss, self).__init__(name=name, **kwargs)
        self.current_average = self.add_weight(name='current_average', initializer='zeros')
        self.number_of_calls = self.add_weight(name='n_calls', initializer='zeros')
    
    def update_state(self, y_true, y_pred=None, sample_weight=None):
        """
        Method that update the metric with a running average.
        
        Args:
            y_true: None, not needed
            y_pred: loss value
        """
        self.number_of_calls.assign_add(1)
        new_average_to_add = tf.divide(tf.subtract(y_true, self.current_average),
                                       self.number_of_calls)        
        self.current_average.assign_add(new_average_to_add)
    
    def result(self):
        return self.current_average


# Custom Layer
class BayesianDense(tfk.layers.Dense):
    """
    Dense layer with Bayesian prior distribution on the weights.
    """
    def __init__(self, units, n_batches, prior_mean=0., prior_scale=1., **kwargs):
        self.prior_mean = prior_mean
        self.prior_scale = prior_scale
        self.n_batches = n_batches
        super(BayesianDense, self).__init__(units=units, **kwargs)
    
    def call(self, inputs):
        self.add_loss(tf.math.divide(self.log_prob_from_prior(self.kernel), self.n_batches))
        self.add_loss(tf.math.divide(self.log_prob_from_prior(self.bias), self.n_batches))
        return super(BayesianDense, self).call(inputs=inputs)

    def log_prob_from_prior(self, weights):
        prior_distr = tfd.Normal(self.prior_mean, self.prior_scale)
        return tf.reduce_sum(-prior_distr.log_prob(weights))


# Define model
input_layer = tfk.Input(shape=(p, ))
out_layer = BayesianDense(units=1, n_batches=n_batches, activation=None)(input_layer)

model = tfk.Model(input_layer, out_layer)

optimizer = tf.optimizers.RMSprop(learning_rate=0.01)


## CHECK START ##
model(X[0:10, :])
model.losses

prior_distr = tfd.Normal(0., 1.)
model.get_weights()[0]
prior_distr.log_prob(model.get_weights()[0])
## CHECK END ##

"""
Define functions to do a train_step
"""
# Prepare the metrics.
train_mse_metric = tfk.metrics.MeanSquaredError(name="mse")
val_mse_metric = tfk.metrics.MeanSquaredError(name="mse")

# And the loss class
train_loss = CustomLoss(name="train_loss")
val_loss = CustomLoss(name="val_loss")

# containers for loss and metrics
history_loss = dict(train=np.zeros([n_epochs]),
                    val=np.zeros([n_epochs]))
history_metrics = dict(train=np.zeros([n_epochs]),
                       val=np.zeros([n_epochs]))


@tf.function
def train_step(x, y):
    # Open a GradientTape to record the operations run
    # during the forward pass, which enables auto-differentiation.
    with tf.GradientTape() as tape:
        output = model(x, training=True)
        loss_value = neg_log_likelihood(y, output)
        # Add the loss coming from the prior distribution
        loss_value += tf.reduce_sum(model.losses)
    # Compute the gradients
    grads = tape.gradient(loss_value, model.trainable_weights)
    optimizer.apply_gradients(zip(grads, model.trainable_weights))
    # Update the metric
    train_mse_metric.update_state(y, output)
    # Update the loss
    train_loss.update_state(y_true=loss_value)
    
    # return the loss
    return loss_value


@tf.function
def val_step(x, y):
    output = model(x, training=False)
    val_loss_value = neg_log_likelihood(y, output)
    val_loss_value += tf.reduce_sum(model.losses)
    # Update the metric
    val_mse_metric.update_state(y, output)
    # Update the loss
    val_loss.update_state(y_true=val_loss_value)
    
    return val_loss_value


# Training loop

start_time = time.time()
for epoch in range(n_epochs):
    
    # Iterate over the batches of the dataset
    for x_batch_train, y_batch_train in data_train:
        train_step(x=x_batch_train, y=y_batch_train)
    
    # At the end of each epoch display the metrics
    train_metric = train_mse_metric.result()
    history_metrics["train"][epoch] = train_metric
    history_loss["train"][epoch] = train_loss.result()
    print("Training mse: %.4f" % (float(train_metric),))


    # Run a validation loop at the end of each epoch.
    for x_batch_val, y_batch_val in data_test:
        val_step(x=x_batch_val, y=y_batch_val)
    val_metric = val_mse_metric.result()
    history_metrics["val"][epoch] = val_metric
    history_loss["val"][epoch] = val_loss.result()
    print("Validation mse: %.4f" % (float(val_metric),))

    # Reset training metrics at the end of each epoch
    train_mse_metric.reset_states()
    val_mse_metric.reset_states()
    # Reset losses at the end of each epoch
    train_mse_metric.reset_states()
    val_mse_metric.reset_states()

end_time = time.time()
comp_time = end_time - start_time
print("Time taken: %.2fs" % (comp_time))


loss_plot(model_history=history_metrics)
loss_plot(model_history=history_loss)

model.get_weights()
