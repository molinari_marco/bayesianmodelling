import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


def loss_plot(model_history, train_sample_size=None, test_sample_size=None,
              label_text="loss"):
    plt.figure()
    
    train_loss = np.array(model_history["train"])
    val_loss = np.array(model_history["val"])
    if (train_sample_size is not None) and (test_sample_size is not None):
        train_loss = train_loss / train_sample_size
        val_loss = val_loss / test_sample_size
        
    x = [x + 1 for x in range(len(train_loss))]
    
    plt.plot(x, train_loss, label=f"Train {label_text}")
    plt.plot(x, val_loss, label=f"Validation {label_text}")
    
    plt.legend(loc="upper right")
    plt.xlabel("epochs")
    plt.ylabel(label_text)
    plt.show()
