import matplotlib.pyplot as plt
import seaborn as sns


def bidimensional_density_plot(weights1, weights2):
    sns.set_style("darkgrid")
    sns.jointplot(data=weights1, x="w0", y="w1", kind='kde', fill=True)
    sns.jointplot(data=weights2, x="w2", y="w3", kind='kde', fill=True)
    plt.show()
