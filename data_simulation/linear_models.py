"""
Data Simulation functions
"""
import numpy as np


def sim_linear_regression(n, p, true_weights=None, seed=1234):
    """
    Simulate a linear regression with p features and n observations.
    """
    np.random.seed(seed)
    X = np.random.randn(n, p).astype(np.float32)
    if true_weights is None:
        true_weights = np.random.choice([1, -1, 2, -1.5, -0.5], size=p, replace=True)
    y = X.dot(true_weights) + np.random.randn(n).astype(np.float32)
    y = np.reshape(y, newshape=[n, 1]).astype(np.float32)

    out = dict(X=X, y=y)
    
    return out, true_weights


def sim_logistic_regression(n, p, true_weights=None, seed=1234):
    """
    Simulate a logistic regression with p features and n observations.
    """
    np.random.seed(seed)
    X = np.random.randn(n, p).astype(np.float32)
    if true_weights is None:
        true_weights = np.random.choice([1, -1, 2, -1.5, -0.5], size=p, replace=True)
    lin_pred = X.dot(true_weights) + np.random.randn(n).astype(np.float32) * 0.5
    y = lin_pred.copy()
    y[lin_pred > 0] = 1.
    y[lin_pred <= 0] = 0.
    y = np.reshape(y, newshape=[n, 1]).astype(np.float32)

    out = dict(X=X, y=y, true_weights=true_weights)

    return out
