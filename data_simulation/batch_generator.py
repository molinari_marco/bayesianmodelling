import numpy as np
import tensorflow as tf


def data_batch_generator(dict_data, batch_size):
    """
    This creates the mini-batch generators for the given input features and label.

    :param batch_size: size of the mini-batches
    :type batch_size: int
    """
    X_train = dict_data["X_train"]
    X_test = dict_data["X_test"]
    y_train = dict_data["y_train"]
    y_test = dict_data["y_test"]
    # SGD parameters
    n_train = X_train.shape[0]
    n_test = X_test.shape[0]
    n_batches = np.ceil(n_train / batch_size)

    # Define TensorFlow Dataset instance - produces mini-batches
    # train set
    data_train = tf.data.Dataset.from_tensor_slices(({'X_train':X_train, 'y_train':y_train}, y_train)).shuffle(
        buffer_size=n_batches * batch_size).batch(batch_size)
    # test set
    data_test = tf.data.Dataset.from_tensor_slices((X_test, y_test)).batch(n_test)

    return dict(data_train=data_train, data_test=data_test)
