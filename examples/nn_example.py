import tensorflow as tf
import tensorflow_probability as tfp
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from sgmcmc.create_mcmc import SGMCMC
from sgmcmc.utility.split_train_test import TrainTestSplit
from data_simulation import linear_models
from sgmcmc import custom_layers, losses, learning_rate_schedule
from sgmcmc.optimizers import RMSpropSGLD
from plots import loss_plot

tfk = tf.keras
tfk.backend.set_floatx("float32")
tfd = tfp.distributions

""" Generate some data from a linear regression """
n = 1000
p = 5
n_train_prop = 0.8
# Split in Train and Test
n_train = int(n_train_prop * n)
n_test = n - n_train

dict_data, true_weights = linear_models.sim_linear_regression(n, p, seed=1234)

# create train-test split
train_test_generator = TrainTestSplit(n=n, n_train=n_train)
train_test_data = train_test_generator.train_test_split(dict_data)

""" mini-batch settings """
batch_size = 128
n_epochs = 50
n_batches = int(np.ceil(n_train / batch_size))
tot_iterations = int(n_epochs * n_batches)
thinning = 2

""" create the batch generators """
# Define TensorFlow Dataset instance - produces mini-batches
# train set
train_generator = tf.data.Dataset.from_tensor_slices(
    ({'x_loc': train_test_data['X']['train']},
     train_test_data['y']['train'])
    ).shuffle(buffer_size=n_batches * batch_size).batch(batch_size)
# test set
test_generator = tf.data.Dataset.from_tensor_slices(
    ({'x_loc': train_test_data['X']['test']},
     train_test_data['y']['test'])).batch(batch_size)

""" Define the prior distributions """
# mean
mean_kernel_prior_distr = tfd.Normal(0, 1)
mean_bias_prior_distr = tfd.Normal(0, 1)

""" Define the model """
mean_input_layer = tfk.Input(shape=(p, ), name='mean_input_layer')
out_layer = custom_layers.BayesianDense(
    units=1,
    n_batches=n_batches,
    kernel_prior=mean_kernel_prior_distr,
    name='mean_layer_1',
    bias_prior=mean_bias_prior_distr,
    activation=None)(mean_input_layer)
model = tfk.Model([mean_input_layer], out_layer)

# Define the distribution of the label
label_distro = losses.GaussianNegLogLikelihood(scale_is_trainable=False, default_scale=0.5)
# label_distro = losses.BernoulliNegLogLikelihood(parametrisation='logits')

# define the learning rate schedule
lr_fn = learning_rate_schedule.CyclicalDecay(
    initial_learning_rate=0.1,
    tot_steps=tot_iterations,
    num_cycles=4
    )

# choose the sampler
sampler = RMSpropSGLD(
    learning_rate=lr_fn,
    rho=0.95)

# Define the MCMC object.
# The exploring and sampling temperatures can be specified
mcmc = SGMCMC(model=model, label_distribution=label_distro, n_epochs=n_epochs,
              data_train_generator=train_generator,
              data_test_generator=test_generator, sampler=sampler, n_batches=n_batches,
              thinning=thinning, lr_schedule=lr_fn, temperature_exploration=0.5, temperature_sampling=1.,
              threshold_collect=0.5, store_running_average=False, store_full_chain=True,
              metrics=tfk.metrics.MeanSquaredError)

# Check the number of effective samples
print(mcmc.threshold_collect)
print(mcmc.n_samples)

# run the chain
mcmc.run_chain()

# Plot the loss and metric
loss_plot.loss_plot(
    model_history=mcmc.history_metrics,
    label_text="metric"
    )

loss_plot.loss_plot(
    model_history=mcmc.history_loss,
    label_text="loss",
    train_sample_size=n * n_train_prop,
    test_sample_size=n * (1-n_train_prop)
)

# Explore full chain
weights = mcmc.posterior_distribution.posterior_samples['mean_layer_1_pos_0']
plt.plot(weights[:, :, 0])
plt.show()

# With seaborn
col_names = [f"w{ii}" for ii in range(p)]
df_weights = pd.DataFrame(data=weights[:, :, 0], columns=col_names)

sns.set_style("darkgrid")
sns.jointplot(data=df_weights, x="w0", y="w1", kind='kde', fill=True)
plt.show()
sns.jointplot(data=df_weights, x="w2", y="w3", kind='kde', fill=True)
plt.show()

# Check the running average and variance
run_ave = mcmc.posterior_distribution.run_ave
run_ave_square = mcmc.posterior_distribution.run_ave_square

kernel_pseudo_posterior_distr = tfd.Normal(run_ave[0], tf.math.sqrt(run_ave_square[1]))
df_pseudo_weights = pd.DataFrame(data=kernel_pseudo_posterior_distr.sample(500).numpy()[:, :, 0], columns=col_names)

# compare
sns.jointplot(data=df_weights, x="w0", y="w1", kind='kde', fill=True)
sns.jointplot(data=df_pseudo_weights, x="w0", y="w1", kind='kde', fill=True)
plt.show()

# predictions on new data
predictions = mcmc.predict(x=dict_data['X_test'])
plt.hist(predictions[:, 0, :])
plt.show()
